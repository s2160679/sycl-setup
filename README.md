# sycl setup

Instruction on setting up a VScode sycl project (CUDA backend) on a Linux operating system (Ubuntu).

## Prerequisites 

* Ubuntu system or WSL (version >= Ubuntu 22.04.1 LTS)
* VScode


## Step 1 

Download or upgrading C++ developing envirnment using following commands: 
```console
sudo apt update
sudo apt -y install cmake pkg-config build-essential
``` 
These command should download cmake gcc and g++ for you. To verify if the installation of these tools command:
```console
whereis cmake pkg-config make gcc g++
```
If the installation is successful you should expect similar result:
```console
cmake: /usr/lib/x86_64-linux-gnu/cmake /usr/lib/cmake /usr/local/lib/cmake /usr/share/cmake
pkg-config: /usr/bin/pkg-config /usr/share/man/man1/pkg-config.1.gz
make: /usr/bin/make /usr/share/man/man1/make.1.gz
gcc: /usr/bin/gcc /usr/lib/gcc /usr/share/gcc /usr/share/man/man1/gcc.1.gz
g++: /usr/bin/g++ /usr/share/man/man1/g++.1.gz
```

## Step 2
Installing basic oneAPI tool chain in [here](https://www.intel.com/content/www/us/en/developer/tools/oneapi/toolkits.html#gs.03mtme) (Notice: The version of the toolchain should be >= 2023.0.0)

## Step 3
Installing CUDA for NVDIA backend:
* install from this [link](https://docs.nvidia.com/cuda/wsl-user-guide/index.html) if you are runing WSL system 
* install from this [link](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html) if you are runing on native Ubuntu system

## Step 4
Installing oneAPI using the [this](https://developer.codeplay.com/products/oneapi/nvidia/download/) link

Extracting the downloaded installer with the follwing command: 
```console
sh <filename>.sh
```
## Step 4
To excuting oneAPI command we need to set the envirnment using the Intel-provided __setvars.sh__ script
```console
source /opt/intel/oneapi/setvars.sh 
```
It is recommonded to source the script in __~/.bashrc__ so you don't need to source it every session

## Step 4
Creating a file test.cpp with the following C++/SYCL code:

``` 
#include <sycl/sycl.hpp>

int main() {
  // Creating buffer of 4 elements to be used inside the kernel code
  sycl::buffer<size_t, 1> Buffer(4);

  // Creating SYCL queue
  sycl::queue Queue;

  // Size of index space for kernel
  sycl::range<1> NumOfWorkItems{Buffer.size()};

  // Submitting command group(work) to queue
  Queue.submit([&](sycl::handler &cgh) {
    // Getting write only access to the buffer on a device.
    sycl::accessor Accessor{Buffer, cgh, sycl::write_only};
    // Executing kernel
    cgh.parallel_for<class FillBuffer>(
        NumOfWorkItems, [=](sycl::id<1> WIid) {
          // Fill buffer with indexes.
          Accessor[WIid] = WIid.get(0);
        });
  });

  // Getting read only access to the buffer on the host.
  // Implicit barrier waiting for queue to complete the work.
  sycl::host_accessor HostAccessor{Buffer, sycl::read_only};

  // Check the results
  bool MismatchFound = false;
  for (size_t I = 0; I < Buffer.size(); ++I) {
    if (HostAccessor[I] != I) {
      std::cout << "The result is incorrect for element: " << I
                << " , expected: " << I << " , got: " << HostAccessor[I]
                << std::endl;
      MismatchFound = true;
    }
  }

  if (!MismatchFound) {
    std::cout << "The results are correct!" << std::endl;
  }

  return MismatchFound;
} 
```

To check if the __dpcpp__ compiler is setup correctly,  in the current terminal session enter the following command.
```console
dpcpp test.cpp -o test
```
If now error occurs run the excutable with the following command.
```console
./test
```
If both the hardware and software is setup correctly. The program should output:
 ```console
The results are correct!
```
The VScode setup can be then installed using following [link](https://codeplay.com/portal/blogs/2023/03/01/debugging-sycl-code-with-dpc-and-visual-studio-code.html).

