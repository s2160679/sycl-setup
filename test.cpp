#include <sycl/sycl.hpp>

void dpcppTask(sycl::queue &q, std::vector<int> &Vect){

    size_t length = Vect.size();

    // Creating buffer
    sycl::buffer Buffer(Vect);

    sycl::range<1> NumOfWorkItems{Buffer.size()};

    // Submitting command group(work) to queue
    q.submit([&](sycl::handler &cgh) {
    // Getting write only access to the buffer on a device.
    sycl::accessor Accessor{Buffer, cgh, sycl::write_only};
    // Executing kernel
    cgh.parallel_for<class FillBuffer>(
        NumOfWorkItems, [=](sycl::id<1> WIid) {
          // Fill buffer with indexes.
          Accessor[WIid] = WIid.get(0);
        });
  });

}

bool checkResult(std::vector<int> &Vec){

  for (size_t I = 0; I < Vec.size(); ++I) {
    if (Vec[I] != I) {
      std::cout << "The result is incorrect for element: " << I
                << " , expected: " << I << " , got: " << Vec[I]
                << std::endl;
      
      return false;
    }
  }

  std::cout << "The results are correct!" << std::endl;
  return true;
}


int main() {


    // for (auto platform : sycl::platform::get_platforms())
    // {
    //     std::cout << "Platform: "
    //               << platform.get_info<sycl::info::platform::name>()
    //               << std::endl;

    //     for (auto device : platform.get_devices())
    //     {
    //         std::cout << "\tDevice: "
    //                   << device.get_info<sycl::info::device::name>()
    //                   << std::endl;
    //     }

    //     // Creating SYCL queue
    //     sycl::queue Queue(platform.get_devices()[0]);
    //     std::vector<int> Vec(4);
    //     dpcppTask(Queue,Vec);
    //     if (checkResult(Vec)) {
    //         std::cout << "The results are correct!" << std::endl;
    //     }

    //     std::cout <<  std::endl;
    // }


    sycl::queue Queue(sycl::gpu_selector_v);

      std::cout << "Running on: "
              << Queue.get_device().get_info<sycl::info::device::name>()
              << std::endl;

      std::vector<int> Vec(4);
      dpcppTask(Queue,Vec);
      checkResult(Vec);









   
  

  return 0;
} 
